import time
import datetime
from epics import caget, caput
import numpy as np
import matplotlib.pyplot as plt

class pyEpicsExperiment:
    """ An example of a synchrotron beamline experiment using pyepics

    This simple example demonstrates the use of pyepics to control stage
    motors and monitor signals by reading and writing to epics process
    variables. It is aimed at beamline scientists, research students and
    researchers alike who are seeking to automate their synchrotron
    beamline experiments.

    WARNING: extreme care should be taken when conducting  beamline
    experiments. There is a risk of damage to equipment through inappropriate
    use of samples stages and beamline shutters. Consult your local controls
    engineer &/or beamline scientist if you are unsure of any part of this
    script. The author accepts no responsibility for any damage caused by
    this script, or any derivative thereof.
    Happy sciencing!

    Requirements:
    - channel access libraries
    These should be present on the machine in which you install the script,
    and headers and libraries in the search path.
    - pyepics module
    The python bindings for channel access commands can be installed using
    the python package index:
    $> sudo pip install pyepics
    - numpy
    For analysis of data.
    - matplotlib
    For visualisation of results.

    Usage: See the main function for example usage.
    """

    def __init__(self):
        """ Initialisation

        You will need to replace these placeholder pv names with those for
        the EPICS database on your beamline.
        """
        self.keithley_current_pv = "xxxxxxxx:Measure"
        self.stage_pv="xxxxxxxx"
        self.shutter_controller_pv="xxxxxx"

    def sampleKeithley(self, sampleTime=3.0):
        """ sample the keithley ammeter
        This method uses caget to periodically read a process variable containing
        the latest current reading from a Keightly ammeter. It assumes your
        beamline already has an IOC reading out the Keithley and writing to
        an EPICS database.
        It returns the raw data, along with some statistics.
        """
        samples=[]
        t0 = time.time()
        while time.time() - t0 < sampleTime:
            time.sleep(0.1)
            sample = caget(self.keithley_current_pv)
            samples.append(sample)
        samples = np.array(samples)
        mean = np.mean(samples)
        stddev = np.std(samples)
        print("mean ", mean, " stddev ", stddev)
        return samples, mean, stddev

    def runRasterScan(self):
        """ Perform a raster scan of the sample.

        This method firstlyreads out the ammeter to determine background
        signal. A raster scan of the sample through the beam is performed by
        manipulating the stage motors using pyepics through the relevant
        process  variables.
        """
        bckGrndSmps, bckGrndMn, bckGrndStd = self.sampleKeithley()
        #The limits of the scan have been reduced for safety.
        Zrange = 1.0 #mm
        Zstep = 0.1 #mm
        Yrange = 1.0 #mm
        Ystep = 0.1 #mm
        LargeCTY = self.stage_pv+":Y"
        LargeCTZ = self.stage_pv+":Z"
        #The motor settings corresponding to centre of sample aligned with beam
        #axis. These should have been determined manually.
        Zcentred = 0.0
        Ycentred = 0.0
        Zstart = Zcentred - Zrange/2.0
        Zstop = Zcentred + Zrange/2.0
        Zpositions = np.arange(Zstart, Zstop, Zstep)
        Ystart = Ycentred - Yrange/2.0
        Ystop = Ycentred + Yrange/2.0
        Ypositions = np.arange(Ystart, Ystop, Ystep)
        currents = []
        currentStdDev = []
        for zpos in Zpositions:
            print("moving to z pos ", zpos)
            caput(LargeCTZ, zpos)
            #Wait for motor to be done moving.
            while caget(LargeCTZ+".DMOV") == 0:
                time.sleep(0.1)
            for ypos in Ypositions:
                print("moving to y pos ", ypos)
                caput(LargeCTY, ypos)
                #Wait for motor to be done moving.
                while caget(LargeCTY+".DMOV") == 0 :
                        time.sleep(0.1)
                #Open the beamline shutter. Wait for confirmation from database
                #that value has been written. NB. does not guarantee shutter is
                #actually open. Should read another monitor for independent
                #confirmation.
                caput(self.shutter_controller_pv+":SHUTTER_OPEN_CMD", 1,\
                      wait=True)
                #Read out the keithley.
                bmOnSmps, bmOnMn, bmOnStd = self.sampleKeithley()
                #Close the beamline shutter.
                caput(self.shutter_controller_pv+":SHUTTER_CLOSE_CMD", 1,\
                      wait=True)
                bmCurrent = bmOnMn - bckGrndMn
                print("current ", bmCurrent)
                currents.append(bmCurrent)
                currentStdDev.append(bmOnStd)
        currents = np.array(currents)
        currentStdDev = np.array(currentStdDev)
        caput(LargeCTY, Ycentred)
        caput(LargeCTZ, Zcentred)
        #save to a numpy array file with compression
        np.savez("rasterimage", a=Ypositions, b=Zpositions, c=currents, \
                 d=currentStdDev)
        #plot results to a .png file
        samplesgrid = currents.reshape(Zpositions.size,Ypositions.size)*1e6
        # Convert raw current reading to microamps and create 2D array for
        # plotting.
        Y,Z = np.meshgrid(Ypositions,Zpositions)
        levels = np.linspace(0, np.max(samplesgrid), 100)
        levels = np.array(levels)
        figImg = plt.figure()
        axImg = figImg.add_subplot("111")
        cb=axImg.contourf(Z, Y, samplesgrid, levels=levels, cmap=plt.cm.hot)
        cbar=figImg.colorbar(cb,format='%.3f')
        cbar.set_label('$I, \mu A$')
        axImg.set_ylabel("z, mm")
        axImg.set_xlabel("y, mm")
        axImg.ticklabel_format(style='sci', scilimits=(0,0), axis='z')
        figImg.savefig("rasterimage.png")
        plt.show()

    def runRadiationHardnessStudy(self):
        """ A method to repeatedly irradiate a sample and monitor signal.

        The method first takes background measurement of the ammeter.
        Then for a given number of irradiations, it opens a shutter,
        samples the ammeter, and closes the shutter.
        Finally, it saves and plots the data.
        """
        bckGrndSmps, bckGrndMn, bckGrndStd = self.sampleKeithley()
        currentMeans = []
        currentStdDevs = []
        currentVals = []
        numberIrradiations = 100
        for n in np.arange(numberIrradiations):
            caput(self.shutter_controller_pv+"SHUTTER_OPEN_CMD", 1, \
                  wait=True)
            bmOnSmps, bmOnMn, bmOnStd = self.sampleKeithley()
            caput(self.shutter_controller_pv+"SHUTTER_CLOSE_CMD", 1, \
                  wait=True)
            bmCurrent = bmOnMn - bckGrndMn
            print("current ", n, " ", bmCurrent, " A")
            currentMeans.append(bmCurrent)
            currentStdDevs.append(bmOnStd)
            currentVals.append(bmOnSmps-bckGrndMn)
        currentMeans = np.array(currentMeans)
        currentStdDevs = np.array(currentStdDevs)
        currentVals = np.array(currentVals)
        #We save the data to a numpy array
        np.savez("radhard", a=currentMeans, b=currentStdDevs, c=currentVals)
        #Plot the average current vs irradiation number.
        fig = plt.figure()
        ax = fig.add_subplot("111")
        ax.set_ylabel("$I, \mu A$")
        ax.set_xlabel("N")
        ax.plot(currentMeans, marker="o", linestyle="None", color="b")
        fig.savefig("radhard.png")
        #The second figure shows the raw data vs sample number (ie. time).
        fig = plt.figure()
        ax = fig.add_subplot("111")
        ax.set_ylabel("$I, \mu A$")
        ax.set_xlabel("N")
        ax.plot(currentVals, marker="o", linestyle="None", color="b")
        fig.savefig("radhardAll.png")
        plt.show()

def main():
    """ Example implementation of pyepicsexperiment class

    Running the script with
    $> python pyepicsexperiment.py results in this method being executed.
    Uncomment the expeirment you are interested in running.
    """
    from pyEpicsExperiment import pyEpicsExperiment
    start=time.time()
    pyEpicsExperiment = pyEpicsExperiment()
    pyEpicsExperiment.runRadiationHardnessStudy()
    #pyEpicsExperiment.runRasterScan()
    stop=time.time()
    scantime = np.int(stop - start)
    scantimehms = str(datetime.timedelta(seconds=scantime))
    print("scan time - h:m:s ", scantimehms)

if __name__ == '__main__' : main()




