# Running a synchrotron beamline experiment using pyepics #
## Introduction ##

This simple script demonstrates the use of pyepics to control stage
motors and monitor signals by reading and writing to epics process
variables. It is aimed at beamline scientists, research students and
researchers alike who are seeking to automate their synchrotron
beamline experiments.

WARNING: extreme care should be taken when conducting  beamline
experiments. There is a risk of damage to equipment through inappropriate
use of samples stages and beamline shutters. Consult your local controls
engineer &/or beamline scientist if you are unsure of any part of this
script. The author accepts no responsibility for any damage caused by
this script, or any derivative thereof.

Happy sciencing!

## Requirements ##
### channel access libraries ###
These should be present on the machine in which you install the script,
and headers and libraries in the search path.
### pyepics module ###
The python bindings for channel access commands can be installed using
the python package index:
```
sudo pip install pyepics
```
### numpy ###
For analysis of data.
### matplotlib ###
For visualisation of results.

## Usage ##
Example usage is given in the main function, which can be run by:
```
#!python
python ./pyEpicsExperiment.py
```
WARNING: This will not work correctly 'out of the box'. You will need to configure your script in accordance with process variable names on your local epics database. Moreover, the scan limits for the raster scan will need to be modified.




